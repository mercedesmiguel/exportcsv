﻿using System;
using Microsoft.Xrm.Sdk;

namespace ISE.SDK.Plugin
{
    public class LocalPluginContext<TEntity, TServiceContext> 
        where TEntity : Entity 
        where TServiceContext : Microsoft.Xrm.Sdk.Client.OrganizationServiceContext
    {

        internal IServiceProvider ServiceProvider
        {
            get;

            private set;
        }

        internal IOrganizationService OrganizationService
        {
            get;

            private set;
        }

        internal IPluginExecutionContext PluginExecutionContext
        {
            get;

            private set;
        }

        internal ITracingService TracingService
        {
            get;

            private set;
        }

        internal TServiceContext ServiceContext
        {
            get;
            private set;
        }

        public LocalPluginContext()
        {
        }

        public void UseSystemAccount()
        {
            IOrganizationServiceFactory factory = (IOrganizationServiceFactory)this.ServiceProvider.GetService(typeof(IOrganizationServiceFactory));
            this.OrganizationService = factory.CreateOrganizationService(null);

            this.ServiceContext = (TServiceContext)Activator.CreateInstance(typeof(TServiceContext), (IOrganizationService)OrganizationService);
        }

        public LocalPluginContext(IServiceProvider serviceProvider)
        {
            if (serviceProvider == null)
            {
                throw new ArgumentNullException("serviceProvider");
            }

            this.ServiceProvider = serviceProvider;

            // Obtain the execution context service from the service provider.
            this.PluginExecutionContext = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));

            // Obtain the tracing service from the service provider.
            this.TracingService = (ITracingService)serviceProvider.GetService(typeof(ITracingService));

            // Obtain the Organization Service factory service from the service provider
            IOrganizationServiceFactory factory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));

            // Use the factory to generate the Organization Service.
            this.OrganizationService = factory.CreateOrganizationService(this.PluginExecutionContext.UserId);

            // create the service data context
            this.ServiceContext = (TServiceContext)Activator.CreateInstance(typeof(TServiceContext), (IOrganizationService) OrganizationService);
        }

        internal void Trace(string message, params object[] args)
        {
            Trace(String.Format(message, args));
        }

        internal void Trace(string message)
        {
            if (String.IsNullOrWhiteSpace(message) || this.TracingService == null)
            {
                return;
            }

            if (this.PluginExecutionContext == null)
            {
                this.TracingService.Trace(message);
            }
            else
            {
                this.TracingService.Trace(
                    "{0}, Correlation Id: {1}, Initiating User: {2}",
                    message,
                    this.PluginExecutionContext.CorrelationId,
                    this.PluginExecutionContext.InitiatingUserId);
            }
        }


        /// <summary>
        /// Returns the post image. Assumes the naming convention 'PostImage'
        /// </summary>
        /// <returns></returns>
        public TEntity GetPostImage()
        {
            return GetImage(ImageType.PostImage);
        }

        /// <summary>
        /// Returns the pre image. Assumes the naming convention 'PreImage'
        /// </summary>
        /// <returns></returns>
        public TEntity GetPreImage()
        {
            return GetImage(ImageType.PreImage);
        }

        /// <summary>
        /// Returns the image from the image collections
        /// </summary>
        /// <param name="imageType"></param>
        /// <returns>The image. Null if none found.</returns>
        public TEntity GetImage(ImageType imageType)
        {
            return GetImage(imageType.ToString(), imageType);
        }

        /// <summary>
        /// Returns the image from the image collections
        /// </summary>
        /// <param name="imageName"></param>
        /// <param name="imageType"></param>
        /// <returns>The image. Null if none found.</returns>
        public TEntity GetImage(string imageName, ImageType imageType)
        {
            EntityImageCollection parameterCollection;
            switch (imageType)
            {
                case ImageType.PostImage:
                    parameterCollection = PluginExecutionContext.PostEntityImages;
                    break;
                case ImageType.PreImage:
                    parameterCollection = PluginExecutionContext.PreEntityImages;
                    break;
                default:
                    throw new ArgumentOutOfRangeException("imageType");
            }

            if (parameterCollection.ContainsKey(imageName) && parameterCollection[imageName] != null)
            {
                return parameterCollection[imageName].ToEntity<TEntity>();
            }
            return null;
        }

        /// <summary>
        /// Returns the target entity
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        public TEntity GetTargetFromParameters()
        {
            if (PluginExecutionContext.InputParameters.ContainsKey("Target") &&
                PluginExecutionContext.InputParameters["Target"] is Entity)
            {
                return ((Entity)PluginExecutionContext.InputParameters["Target"]).ToEntity<TEntity>();
            }
            return null;
        }
    }

    public enum ImageType
    {
        PostImage,
        PreImage
    }
}