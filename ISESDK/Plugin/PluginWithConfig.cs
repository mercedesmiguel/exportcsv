﻿using ISE.SDK.Configuration;
using Microsoft.Xrm.Sdk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ISE.SDK.Plugin
{
    /// <summary>
    /// Abstract plugin class for plugins that support configuration, using and XML configuraiton string.
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    /// <typeparam name="TConfig"></typeparam>
    /// <typeparam name="TServiceContext"></typeparam>
    /// <typeparam name="TPluginContext"></typeparam>
    public abstract class PluginWithConfig<TEntity, TServiceContext, TConfig, TPluginContext> : Plugin<TEntity, TServiceContext, TPluginContext> 
        where TEntity : Entity
        where TServiceContext : Microsoft.Xrm.Sdk.Client.OrganizationServiceContext
		where TConfig : ConfigurationBase
        where TPluginContext : LocalPluginContext<TEntity, TServiceContext>
    {
        protected readonly TConfig _config;

        internal PluginWithConfig(Type childClassName) : this(string.Empty, string.Empty, childClassName)
		{
		    
		}

		internal PluginWithConfig(string unsecure, Type childClassName) : this(unsecure, string.Empty, childClassName)
		{
		    
		}

		internal PluginWithConfig(string unsecure, string secure, Type childClassName) : base(childClassName)
		{
            if (!string.IsNullOrEmpty(unsecure))
            {
                _config = ConfigurationSerializer.Deserialize<TConfig>(unsecure);
            }
		}

        public TConfig Config
        {
            get { return _config; }
        }
    }
}
