using System;
using System.Xml;
using System.Xml.Serialization;
using Microsoft.Xrm.Sdk.Client;
using System.ServiceModel.Description;
using Microsoft.Xrm.Client;
using Microsoft.Xrm.Client.Services;
using Microsoft.Xrm.Sdk;

namespace ISE.SDK.Tools
{
    public class CrmConnector
    {
        private OrganizationService _organizationService;
        public IOrganizationService OrganizationService
        {
            get { return _organizationService; }
        }

        private CrmOrganizationServiceContext _organizationServiceContext;
        public OrganizationServiceContext OrganizationServiceContext
        {
            get { return _organizationServiceContext; }
        }

        public void OpenFromFile(string configFile)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(CrmConnectorConfiguration));
            XmlReader xmlReader = XmlReader.Create(configFile);
            CrmConnectorConfiguration config = (CrmConnectorConfiguration)xmlSerializer.Deserialize(xmlReader);

            Open(config);
        }

        public void WriteToFile(string configFile)
        {
            CrmConnectorConfiguration crmConnectConfig = new CrmConnectorConfiguration();
            crmConnectConfig.CrmConnectionString = "";
            crmConnectConfig.CrmUrl = "";
            crmConnectConfig.Domain = "printer";
            crmConnectConfig.UserName = "y627";
            crmConnectConfig.Password = "%Lampe+gin=3wuensche";
            crmConnectConfig.PerformAttributeUpdate = false;
            crmConnectConfig.DeleteTargetsAdditionalPlugins = false;
            crmConnectConfig.Url =  new Uri("http://ginswfh/ginswfh");

            XmlSerializer xmlSerializer = new XmlSerializer(typeof(CrmConnectorConfiguration));
            XmlWriter xmlWriter = XmlWriter.Create(configFile);
            xmlSerializer.Serialize(xmlWriter, crmConnectConfig);
        }


        public void Open(CrmConnectorConfiguration entity2XmlConfiguration)
        {
            if (!string.IsNullOrEmpty(entity2XmlConfiguration.CrmConnectionString))
            {
                Open(entity2XmlConfiguration.CrmConnectionString);
            }
            else if (!string.IsNullOrEmpty(entity2XmlConfiguration.UserName))
            {
                Open(entity2XmlConfiguration.Url, entity2XmlConfiguration.Domain, entity2XmlConfiguration.UserName, entity2XmlConfiguration.Password);
            }
            else
            {
                Open(entity2XmlConfiguration.Url);
            }
        }

        public void Open(Uri url)
        {
            ClientCredentials credentials = new ClientCredentials();
            credentials.Windows.ClientCredential = System.Net.CredentialCache.DefaultNetworkCredentials;
            
            Open(string.Format("Url={0}", url));
        }

        public void Open(Uri url, string domain, string userName, string password)
        {
            Open(string.Format("Url={0}; Domain={1}; Username={2}; Password={3}", url, domain, userName, password));
        }

        public void Open(string connectionString)
        {
            Console.WriteLine("Open {0}", connectionString);
            var connection = CrmConnection.Parse(connectionString);
            _organizationService = new OrganizationService(connection);
            _organizationServiceContext = new CrmOrganizationServiceContext(connection);
        }
    }
}
