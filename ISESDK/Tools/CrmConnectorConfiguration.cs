using System;
using System.Xml.Serialization;

namespace ISE.SDK.Tools  
{
    public class CrmConnectorConfiguration
    {
        [XmlIgnore]
        public Uri Url
        {
            get
            {
                return new Uri(CrmUrl);
            }
            set { CrmUrl = value.ToString(); }
        }

        public string CrmUrl { get; set; }

        public string UserName { get; set; }
        public string Domain { get; set; }
        public string Password { get; set; }

        public string CrmConnectionString { get; set; }

        public bool DeleteTargetsAdditionalPlugins { get; set; }
        public bool PerformAttributeUpdate { get; set; }
    }
}
