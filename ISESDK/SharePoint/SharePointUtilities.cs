﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Ise.SharePoint.DocumentIntegration.Xrm;

namespace ISE.SDK.SharePoint
{
    /// <summary>
    /// Utility class for functionality related to CRM SharePoint functionality
    /// </summary>
    public sealed class SharePointUtilities
    {
        /// <summary>
        /// Determines a target URL based on the document location
        /// </summary>
        /// <param name="siteUrl"></param>
        /// <param name="location"></param>
        /// <param name="service"></param>
        /// <returns></returns>
        public static string DetermineTargetUrl(string siteUrl, SharePointDocumentLocation location, IOrganizationService service)
        {
            string documentLocationUrl;

            // Determine between absolute and relative URLs
            if (location.AbsoluteURL != null)
            {
                documentLocationUrl = ReplaceSiteUrl(location.AbsoluteURL, siteUrl);
            }
            else
            {
                // for relative URLs prepend the name of the parent location
                var parentLocation =
                    service.Retrieve(SharePointDocumentLocation.EntityLogicalName,
                                                               location.ParentSiteOrLocation.Id,
                                                               new ColumnSet("relativeurl")).ToEntity<SharePointDocumentLocation>();

                documentLocationUrl = siteUrl + "/" + parentLocation.RelativeUrl + "/" + location.RelativeUrl;
            }

            return documentLocationUrl;
        }

        /// <summary>
        /// Replaces the servername of the original url with the servername of the second url and
        /// leaves the path in the URL unchanged
        /// </summary>
        /// <param name="original">the original url</param>
        /// <param name="serverUrl">the url from which to take the servername</param>
        /// <returns>the modified url</returns>
        public static string ReplaceSiteUrl(string original, string serverUrl)
        {
            string regexPattern = @"^(?<s1>(?<s0>[^:/?#]+):)?(?<a1>"
                                  + @"//(?<a0>[^/?#]*))?(?<p0>[^?#]*)";

            Regex serverRegex = new Regex(regexPattern, RegexOptions.ExplicitCapture);
            Match origMatch = serverRegex.Match(original);
            string origServerName = origMatch.Groups["a0"].Value;

            Match siteMatch = serverRegex.Match(serverUrl);
            string siteServerName = siteMatch.Groups["a0"].Value;

            return original.Replace(origServerName, siteServerName);
        }

        /// <summary>
        /// Escapes characters in the filename that are not allowed in sharepoint.
        /// </summary>
        /// <param name="filename">the original filename</param>
        /// <returns>the filename with escape characters</returns>
        public static string EscapeFilename(string filename)
        {
            return Regex.Replace(filename, "[\"<>|#{}%~://" + Regex.Escape("?") + Regex.Escape("*") + Regex.Escape("&") + Regex.Escape("\\") + "]", "-");
        }
    }
}
