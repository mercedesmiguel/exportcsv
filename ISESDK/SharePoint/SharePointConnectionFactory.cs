﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Security.Principal;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Security;
using ISE.SDK.SharePoint.SPCopy;
using ISE.SDK.SharePoint.SPLists;

namespace ISE.SDK.SharePoint
{
    /// <summary>
    /// This class is responsible for the establishment and management of
    /// connections to the SharePoint Server for usage of SharePoint Web Services.
    /// </summary>
    public class SharePointConnectionFactory : IDisposable
    {
        /// <summary>
        /// The relative Url to the Lists web service on a SharePoint site.
        /// </summary>
        private const string ListsWebServicePath = "/_vti_bin/Lists.asmx";

        /// <summary>
        /// The relative Url to the Lists web service on a SharePoint site.
        /// </summary>
        private const string CopyWebServicePath = "/_vti_bin/Copy.asmx";

        /// Configuration names for sharepoint user credentials
        public const string SHAREPOINT_USER_NAME = "user";
        public const string SHAREPOINT_USER_DOMAIN = "domain";
        public const string SHAREPOINT_USER_PASSWORD = "password";
        public const string SHAREPOINT_SITE_URL = "siteurl";

        /// <summary>
        /// Dictionary containing the necessary configuration values for the SharePoint connection
        /// </summary>
        private readonly Dictionary<string, string> _configuration;

        /// <summary>
        /// Internally cached client reference for the Lists Web Service
        /// </summary>
        private ListsSoapClient _listsClient = null;

        /// <summary>
        /// Internally cached client reference for the Copy Web Service
        /// </summary>
        private CopySoapClient _copyClient = null;

        /// <summary>
        /// Determines which transport type to use
        /// </summary>
        private bool _useHttps = false;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="configuration"></param>
        public SharePointConnectionFactory(Dictionary<string, string> configuration, bool useHttps = false)
        {
            _configuration = configuration;
            _useHttps = useHttps;
        }

        /// <summary>
        /// Property for the Lists Web Service client reference
        /// </summary>
        public ListsSoapClient ListsClient { get { return (_listsClient ?? (_listsClient = InstantiateListsSoapClient())); } }

        /// <summary>
        /// Property for the Copy Web Service client reference
        /// </summary>
        public CopySoapClient CopyClient { get { return (_copyClient ?? (_copyClient = InstantiateCopySoapClient())); } }

        /// <summary>
        /// Creates a client instance for the SharePoint Lists Web Service
        /// </summary>
        /// <returns>the SharePoint client object</returns>
        private ListsSoapClient InstantiateListsSoapClient()
        {
            Binding binding = (_useHttps ? GetHttpsBinding() : GetBinding());

            ListsSoapClient client = new ListsSoapClient(binding,
                                                            new EndpointAddress(
                                                                _configuration[SHAREPOINT_SITE_URL] +
                                                                ListsWebServicePath));

            client.ClientCredentials.Windows.AllowedImpersonationLevel = TokenImpersonationLevel.Impersonation;
            client.ClientCredentials.Windows.ClientCredential = GetCredentials(); ;

            return client;
        }

        /// <summary>
        /// Creates a client instance for the SharePoint Copy Web Service
        /// </summary>
        /// <returns>the SharePoint client object</returns>
        private CopySoapClient InstantiateCopySoapClient()
        {
            Binding binding = (_useHttps ? GetHttpsBinding() : GetBinding());

            CopySoapClient client = new CopySoapClient(binding,
                                                            new EndpointAddress(
                                                                _configuration[SHAREPOINT_SITE_URL] +
                                                                CopyWebServicePath));

            client.ClientCredentials.Windows.AllowedImpersonationLevel = TokenImpersonationLevel.Impersonation;
            client.ClientCredentials.Windows.ClientCredential = GetCredentials(); ;

            return client;
        }

        /// <summary>
        /// Builds a default binding as needed for the netzhaus environment
        /// </summary>
        /// <returns></returns>
        private Binding GetBinding()
        {
            BasicHttpBinding binding = new BasicHttpBinding();
            binding.Security.Mode = BasicHttpSecurityMode.TransportCredentialOnly;
            binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Ntlm;
            binding.Security.Transport.ProxyCredentialType = HttpProxyCredentialType.Ntlm;
            binding.Security.Message.ClientCredentialType = BasicHttpMessageCredentialType.UserName;
            binding.Security.Message.AlgorithmSuite = SecurityAlgorithmSuite.Default;
            binding.UseDefaultWebProxy = true;

            return binding;
        }

        /// <summary>
        /// Builds a default binding as needed for the netzhaus environment
        /// </summary>
        /// <returns></returns>
        private Binding GetHttpsBinding()
        {
            BasicHttpBinding binding = new BasicHttpBinding();
            binding.Security.Mode = BasicHttpSecurityMode.Transport;
            binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Ntlm;

            return binding;
        }

        /// <summary>
        /// Creates the credentials for the connection based on the configuration
        /// </summary>
        /// <returns></returns>
        private NetworkCredential GetCredentials()
        {
            return new NetworkCredential(_configuration[SHAREPOINT_USER_NAME],
                                         _configuration[SHAREPOINT_USER_PASSWORD],
                                         _configuration[SHAREPOINT_USER_DOMAIN]);
        }

        /// <summary>
        /// Closes all client connections
        /// </summary>
        public void Dispose()
        {
            if (_listsClient != null)
            {
                _listsClient.Close();
            }

            if (_copyClient != null)
            {
                _copyClient.Close();
            }
        }
    }
}
