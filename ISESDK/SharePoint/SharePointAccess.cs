﻿using System.Linq;
using System.Net;
using System.Xml;
using System.Xml.Linq;
using ISE.SDK.SharePoint.SPCopy;
using ISE.SDK.SharePoint.SPLists;
using Microsoft.Xrm.Sdk;
using System.Collections.Generic;
using System;
using ISE.SDK.Tools;
using Ise.SharePoint.DocumentIntegration.Xrm;

namespace ISE.SDK.SharePoint
{
    public class SharePointAccess
    {
        /// <summary>
        /// Upload the given file to SharePoint
        /// </summary>
        /// <param name="fileContent"></param>
        /// <param name="fileName"></param>
        /// <param name="siteUrl">The SharePoint SiteUrl</param>
        /// <param name="sharePointFolder"></param>
        /// <param name="user">The sharepoint user name</param>
        /// <param name="password">the sharepoint user password</param>
        /// <param name="domain">the sharepoint user domain</param>
        public static string StoreFileInSharePoint(byte[] fileContent, string fileName, string siteUrl, string sharePointFolder,
                                                 string user, string password, string domain)
        {
            string result = string.Empty;

            using (
                var factory =
                    new SharePointConnectionFactory(new Dictionary<string, string>
                        {
                            {SharePointConnectionFactory.SHAREPOINT_USER_NAME, user},
                            {SharePointConnectionFactory.SHAREPOINT_USER_DOMAIN, domain},
                            {SharePointConnectionFactory.SHAREPOINT_USER_PASSWORD, password},
                            {SharePointConnectionFactory.SHAREPOINT_SITE_URL, siteUrl}
                        }, siteUrl.Contains("https://")))
            {
                result = UploadDocument(sharePointFolder, fileName, fileContent, factory.CopyClient);
            }

            return result;
        }

        /// <summary>
        /// Upload the given file to SharePoint
        /// </summary>
        /// <param name="folderName"></param>
        /// <param name="siteUrl">The SharePoint SiteUrl</param>
        /// <param name="listName"></param>
        /// <param name="user">The sharepoint user name</param>
        /// <param name="password">the sharepoint user password</param>
        /// <param name="domain">the sharepoint user domain</param>
        /// <param name="tracingService">the tracing service reference</param>
        public static string CreateFolderInSharePoint(string folderName, string siteUrl,
                                                      string listName,
                                                      string user, string password, string domain,
                                                      ITracingService tracingService = null)
        {
            string result = string.Empty;

            using (
                var factory =
                    new SharePointConnectionFactory(new Dictionary<string, string>
                        {
                            {SharePointConnectionFactory.SHAREPOINT_USER_NAME, user},
                            {SharePointConnectionFactory.SHAREPOINT_USER_DOMAIN, domain},
                            {SharePointConnectionFactory.SHAREPOINT_USER_PASSWORD, password},
                            {SharePointConnectionFactory.SHAREPOINT_SITE_URL, siteUrl}
                        }, siteUrl.Contains("https://")))
            {
                if (tracingService != null)
                {
                    tracingService.Trace("ISE.SDK.SharePoint.SharePointAccess.CreateFolderInSharePoint({0},{1})",
                                         listName, folderName);
                }
                result = CreateFolder(listName, folderName, factory.ListsClient, tracingService);
            }

            return result;
        }

        /// <summary>
        /// Get All files from a SharePoint List 
        /// </summary>
        /// <param name="fileContent"></param>
        /// <param name="fileName"></param>
        /// <param name="siteUrl">The SharePoint SiteUrl</param>
        /// <param name="sharePointFolder"></param>
        /// <param name="user">The sharepoint user name</param>
        /// <param name="password">the sharepoint user password</param>
        /// <param name="domain">the sharepoint user domain</param>
        public static List<SharePointListItem> GetNewItemsFromSharePoint(string listName, DateTime? since,
            string siteUrl, string user, string password, string domain, ITracingService tracingService = null)
        {
            List<SharePointListItem> result = null;

            using (
                var factory =
                    new SharePointConnectionFactory(new Dictionary<string, string>
                        {
                            {SharePointConnectionFactory.SHAREPOINT_USER_NAME, user},
                            {SharePointConnectionFactory.SHAREPOINT_USER_DOMAIN, domain},
                            {SharePointConnectionFactory.SHAREPOINT_USER_PASSWORD, password},
                            {SharePointConnectionFactory.SHAREPOINT_SITE_URL, siteUrl}
                        }, siteUrl.Contains("https://")))
            {
                if (tracingService != null)
                {
                    tracingService.Trace("ISE.SDK.SharePoint.SharePointAccess.GetNewItemsFromSharePoint({0},{1})",
                                         listName);
                }

                result =
                    GetNewListItems(listName, since, siteUrl, factory.ListsClient, tracingService);
            }

            return result;
        }

        /// <summary>
        /// This method allows to create a new folder in SharePoint
        /// </summary>
        /// <param name="listName">Name of the list to create the folder in</param>
        /// <param name="folderName">name of the folder to create</param>
        /// <param name="listsClient">the SharePoint List client</param>
        /// <returns></returns>
        internal static string CreateFolder(string listName, string folderName, ListsSoapClient listsClient, ITracingService tracingService)
        {
            // Create Folder inside a list
            XElement batchRequest = new XElement("Batch",
                new XAttribute("OnError", "Continue"),
                new XElement("Method",
                    new XAttribute("ID", 1),
                    new XAttribute("Cmd", "New"),
                    new XElement("Field",
                        new XAttribute("Name", "FSObjType"),
                        1),
                    new XElement("Field",
                        new XAttribute("Name", "BaseName"),
                        folderName),
                    new XElement("Field",
                        new XAttribute("Name", "ID"),
                        "New")
                )
            );

            if (tracingService != null)
            {
                tracingService.Trace("ISE.SDK.SharePoint.SharePointAccess.CreateFolder() : {0}:{1}", listName, batchRequest.ToString());
            }

            XElement node1 = listsClient.UpdateListItems(listName, batchRequest);

            if (tracingService != null)
            {
                tracingService.Trace("ISE.SDK.SharePoint.SharePointAccess.CreateFolder() : Result = {0}", node1.ToString());
            }

            if (node1.Descendants("ErrorCode").Any(node => !node.Value.Equals("0x00000000")))
            {
                throw new InvalidPluginExecutionException(
                    string.Format("There were errors during the creation of the folder {0}: ErrorCode = {1}", folderName,
                                  node1.Descendants("ErrorCode")
                                       .Where(node => !node.Value.Equals("0x00000000"))
                                       .Select(node => node.Value)));
            }

            return node1.ToString();
        }

        /// <summary>
        /// Copies a document from one SharePoint location to another one
        /// </summary>
        /// <param name="copyClient">the copy web service proxy</param>
        /// <param name="fileName">the document to be copied</param>
        /// <param name="sharePointUrl">the URL to which the document should be copied</param>
        /// <param name="fileContent">the file to be uploaded</param>
        /// <returns></returns>
        internal static string UploadDocument(string sharePointUrl, string fileName, byte[] fileContent,
                                              CopySoapClient copyClient)
        {
            // Perform Copy operation
            CopyResult[] copyResults;

            copyClient.CopyIntoItems(fileName,
                                     new[] { sharePointUrl + "/" + WebUtility.HtmlEncode(fileName) }, new FieldInformation[] { },
                                     fileContent, out copyResults);

            if (copyResults.Any())
            {
                if (!CopyErrorCode.Success.Equals(copyResults[0].ErrorCode))
                {
                    throw new InvalidPluginExecutionException(
                        string.Format("When copying the document \"{0}\" from list \"{1}\": {2}:{3}.",
                                      fileName, sharePointUrl, copyResults[0].ErrorCode, copyResults[0].ErrorMessage));
                }
            }

            return string.Format("Uploaded document \"{0}\" to \"{1}\".",
                                                        fileName,
                                                        sharePointUrl);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="listName"></param>
        /// <param name="since"></param>
        /// <param name="listsClient"></param>
        /// <param name="tracingService"></param>
        /// <returns></returns>
        internal static List<SharePointListItem> GetNewListItems(string listName, DateTime? since, string siteUrl,
            ListsSoapClient listsClient, ITracingService tracingService)
        {
            //Define the required Fields
            XElement viewFields = XElement.Parse("<ViewFields xmlns='http://schemas.microsoft.com/sharepoint/soap/'>" +
                "<FieldRef Name='DocIcon' />" +
                "<FieldRef Name='ContentType' />" +
                "<FieldRef Name='LinkFilename' />" +
                "<FieldRef Name='Modified' />" +
                "<FieldRef Name='Editor' />" +
                "</ViewFields>");

            //Define the Conditions
            XElement contains = XElement.Parse("<Eq> <FieldRef Name='ContentType' /> <Value Type='Text'>Document</Value> </Eq> ");
                
                //XElement.Parse("<IsNotNull>" +
                //"<FieldRef Name='DocIcon' />" +
                //"</IsNotNull>");
            
            //"2010-06-18T00:00:00Z"
            XElement ndListChanges = listsClient.GetListItemChanges(listName, viewFields, 
                (since != null ? since.Value.ToString("s") : null), contains);

            List<SharePointListItem> results = null;
            if (ndListChanges != null)
            {
                results =
                    (from elem in ndListChanges.Elements().Descendants()
                     select new SharePointListItem()
                     {
                         Name = (elem.Attribute("ows_LinkFilename") != null)? elem.Attribute("ows_LinkFilename").Value : String.Empty,
                         URL = elem.Attribute("ows_FileRef").Value,
                         ID = elem.Attribute("ows_ID").Value,
                         ContentType = (elem.Attribute("ows_ContentType") != null) ? elem.Attribute("ows_ContentType").Value : String.Empty,
                         CreatedOn = DateTime.Now
                     }).ToList();
            }

            return results;
        }
    }
}
