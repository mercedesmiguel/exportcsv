﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ISE.SDK.SharePoint
{
    public class SharePointListItem
    {
        public string URL { get; set; }
        public string ID { get; set; }
        public string Name { get; set; }
        public string ContentType { get; set; }
        public DateTime CreatedOn { get; set; }
    }
}
