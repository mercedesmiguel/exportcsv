﻿using System.Runtime.Serialization;

namespace ISE.SDK.Configuration
{
    [DataContract]
    public class DefaultAttributeValue
    {
        [DataMember(Order = 1)]
        public string AttributeName { get; set; }

        [DataMember(Order = 2)]
        public AttributeType Type { get; set; }

        [DataMember(Order = 3)]
        public string Value { get; set; }
    }

    [DataContract]
    public enum AttributeType
    {
        [EnumMember]
        String,
        [EnumMember]
        Int,
        [EnumMember]
        Money,
        [EnumMember]
        Lookup,
        [EnumMember]
        OptionSet,
        [EnumMember]
        Bool,
        [EnumMember]
        Double,
        [EnumMember]
        Decimal,
    }
}