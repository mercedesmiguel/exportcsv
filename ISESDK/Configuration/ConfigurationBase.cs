﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace ISE.SDK.Configuration
{
    [DataContract]
    public class ConfigurationBase
    {
        List<string> _restrictedToValues = new List<string>();

        [DataMember(IsRequired = false, Order = 0, EmitDefaultValue = false)]
        public bool IsDisabled { get; set; }

        [XmlElement(IsNullable = true)]
        public string RestrictingField { get; set; }

        [XmlArrayItem(ElementName = "RestrictedToValue", IsNullable = true, Type = typeof(string))]
        [XmlArray]
        public List<string> RestrictedToValues
        {
            get { return this._restrictedToValues; }
            set { this._restrictedToValues = value; }
        }
    }
}
