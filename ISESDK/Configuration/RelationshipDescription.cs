﻿using System.Runtime.Serialization;

namespace ISE.SDK.Configuration
{
    [DataContract]
    public class RelationshipDescription
    {
        [DataMember(Order = 1)]
        public string RelationshipName { get; set; }

        [DataMember(Order = 2)]
        public string SourceFieldName { get; set; }

        [DataMember(Order = 3)]
        public string TargetFieldName { get; set; }

        [DataMember(Order = 4)]
        public string TargetEntityName { get; set; }
    }
}
