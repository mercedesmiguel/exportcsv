﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace ISE.SDK.Configuration
{
    [DataContract]
    public class AttributeMap
    {
        [DataMember(Order = 1)]
        public string SourceAttributeName { get; set; }
        
        [DataMember(Order = 2)]
        public string TargetAttributeName { get; set; }

        [DataMember(Order = 3, IsRequired = false, EmitDefaultValue = false)]
        public bool DoNotWriteNull { get; set; }
    }
}
