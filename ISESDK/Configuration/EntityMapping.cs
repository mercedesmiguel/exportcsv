﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace ISE.SDK.Configuration
{
    [DataContract]
    public class EntityMap : ConfigurationBase
    {
        [DataMember(Order = 1)]
        public string SourceEntityLogicalName { get; set; }

        [DataMember(Order = 2)]
        public string TargetEntityLogicalName { get; set; }

        [DataMember(Order = 3)]
        public List<AttributeMap> AttributeMaps { get; set; }

        [DataMember(Order = 4)]
        public List<RelationshipDescription> RelationshipDescriptions { get; set; }
    }
}
