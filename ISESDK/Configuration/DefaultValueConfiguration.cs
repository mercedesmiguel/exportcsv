﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace ISE.SDK.Configuration
{
    [DataContract]
    public class DefaultValueConfiguration : ConfigurationBase
    {
        [DataMember(Order = 1)]
        public List<DefaultAttributeValue> DefaultAttributeValues { get; set; }
    }
}
