﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ISE.SDK.Configuration
{
    [DataContract]
    public class EntityTreeConfiguration : EntityTreeNodeConfiguration
    {
        [DataMember(Order = 4)]
        public string RootEntityName { get; set; }
    }

    [DataContract]
    public class EntityTreeNodeConfiguration : ConfigurationBase
    {
        /// <summary>
        /// The name of the field on the entity that references the root entity
        /// </summary>
        [DataMember(Order = 1)]
        public string RootEntityReferenceField { get; set; }

        [DataMember(Order = 2)]
        public EntityMap CopyInstructions { get; set; }

        [DataMember(Order = 3)]
        public List<EntityTreeNodeConfiguration> ChildEntities { get; set; }
    }
}
