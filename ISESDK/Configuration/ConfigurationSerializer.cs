﻿using System.IO;
using System.Runtime.Serialization;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace ISE.SDK.Configuration
{
    public static class ConfigurationSerializer
    {
        public static string Serialize<T>(T source)
        {
            using (var ms = new MemoryStream())
            {
                var serializer = new DataContractSerializer(typeof(T));
                //var xdw = XmlDictionaryWriter.CreateTextWriter(ms, Encoding.UTF8);
                serializer.WriteObject(ms, source);
                using (var sr = new StreamReader(ms))
                {
                    ms.Position = 0;
                    string readToEnd = sr.ReadToEnd();
                    return readToEnd;
                }
            }
        }

        public static T Deserialize<T>(string source)
        {
            var serializer = new DataContractSerializer(typeof(T));
            var ms = new MemoryStream(Encoding.UTF8.GetBytes(source));
            ms.Position = 0;
            var reader = XmlDictionaryReader.CreateTextReader(ms, new XmlDictionaryReaderQuotas {MaxStringContentLength = 100000});
            return (T)serializer.ReadObject(reader);
        }
    }
}