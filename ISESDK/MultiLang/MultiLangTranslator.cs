using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ISE.SDK.Plugin;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Query;

namespace ISE.SDK.MultiLang
{
    /// <summary>
    /// Implements a cached notification provider.
    /// The cache is implemented using singleton pattern.
    /// </summary>
    public class MultiLangTranslator
    {
        /// <summary>
        /// Single instance of MultiLangTranslator
        /// </summary>
        public static MultiLangTranslator Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (syncRoot)
                    {
                        if (instance == null)
                            instance = new MultiLangTranslator();
                    }
                }

                return instance;
            }
        }

        /// <summary>
        /// Cache variable. The key is composed of [message-key, lcid]. The value is the actual message.
        /// </summary>
        private ConcurrentDictionary<Tuple<string, int>, string> _cache = new ConcurrentDictionary<Tuple<string, int>, string>();

        /// <summary>
        /// Backing field for the MultiLangTranslator instance
        /// </summary>
        private static volatile MultiLangTranslator instance;

        /// <summary>
        /// Multi-Thread helper fields
        /// </summary>
        private static object syncRoot = new Object();

        /// <summary>
        /// Private constructor to have singleton pattern
        /// </summary>
        private MultiLangTranslator() { }

        /// <summary>
        /// Returns a notification in the current user's language
        /// This method uses a cache.
        /// </summary>
        /// <param name="key">The message key.</param>
        /// <param name="defaulttext">Text that is returned, if no translation could be found.</param>
        /// <param name="pluginContext">The local plugin context. This is used to query dynamics crm and to identfy the user's language.</param>
        /// <param name="param">Objects that are applied to a string.format() call on the found message.</param>
        /// <returns>The notification or a generic message, if no notification was returned.</returns>
        public string GetTranslation(string key, string defaulttext,
                                     LocalPluginContext<Entity, OrganizationServiceContext> pluginContext,
                                     params object[] param)
        {
            return GetTranslation(key, defaulttext, pluginContext.PluginExecutionContext.InitiatingUserId,
                                  pluginContext.OrganizationService, pluginContext.TracingService, param);
        }

        /// <summary>
        /// Returns a notification in the current user's language
        /// This method uses a cache.
        /// </summary>
        /// <param name="key">The message key.</param>
        /// <param name="defaulttext">Text that is returned, if no translation could be found.</param>
        /// <param name="pluginContext">The local plugin context. This is used to query dynamics crm and to identfy the user's language.</param>
        /// <param name="param">Objects that are applied to a string.format() call on the found message.</param>
        /// <returns>The notification or a generic message, if no notification was returned.</returns>
        public string GetTranslation<TEntity>(string key, string defaulttext,
                                     LocalPluginContext<TEntity, OrganizationServiceContext> pluginContext,
                                     params object[] param)
            where TEntity : Entity
        {
            return GetTranslation(key, defaulttext, pluginContext.PluginExecutionContext.InitiatingUserId,
                                  pluginContext.OrganizationService, pluginContext.TracingService, param);
        }


        /// <summary>
        /// Returns a notification in the current user's language
        /// This method uses a cache.
        /// </summary>
        /// <param name="key">The message key.</param>
        /// <param name="defaulttext">Text that is returned, if no translation could be found.</param>
        /// <param name="initiatingUserId">The id of the user that initiated the call.</param>
        /// <param name="organizationService">The organization service.</param>
        /// <param name="tracingService">The tracing service</param>
        /// <param name="param">Objects that are applied to a string.format() call on the found message.</param>
        /// <returns>The notification or a generic message, if no notification was returned.</returns>
        public string GetTranslation(string key, string defaulttext, Guid initiatingUserId, IOrganizationService organizationService, ITracingService tracingService, params object[] param)
        {
            var languageCode = RetrieveUserUILanguageCode(organizationService, initiatingUserId);

            QueryExpression multilangExpression = new QueryExpression("ise_multilangtext");
            multilangExpression.Criteria.AddCondition("ise_key", ConditionOperator.NotNull);
            multilangExpression.Criteria.AddCondition("ise_key", ConditionOperator.Equal, key);
            multilangExpression.Criteria.AddCondition("ise_language", ConditionOperator.NotNull);
            multilangExpression.Criteria.AddCondition("ise_language", ConditionOperator.Equal, languageCode);
            multilangExpression.ColumnSet.AddColumn("ise_text");

            var multilangTexts = organizationService.RetrieveMultiple(multilangExpression);

            if (multilangTexts.Entities.Count == 0)
            {
                tracingService.Trace(
                    string.Format(
                        "No message found for label: '{0}' and language '{1}'.\nThis means that a configuration is missing. However, this error has no impact on the requested operation.",
                        key,
                        languageCode));

                return string.Format(defaulttext, param);
            }

            return string.Format(multilangTexts.Entities.FirstOrDefault().Attributes["ise_text"].ToString(), param);
        }

        /// <summary>
        /// Gets the uilanguageid from the usersettings
        /// </summary>
        /// <param name="service">Instance of the Organization Service</param>
        /// <param name="userId">Id of the user to get the lanuage for</param>
        /// <returns>User's uilanguageid</returns>
        private int RetrieveUserUILanguageCode(IOrganizationService service, Guid userId)
        {
            QueryExpression userSettingsQuery = new QueryExpression("usersettings");
            userSettingsQuery.ColumnSet.AddColumns("uilanguageid", "systemuserid");
            userSettingsQuery.Criteria.AddCondition("systemuserid", ConditionOperator.Equal, userId);
            EntityCollection userSettings = service.RetrieveMultiple(userSettingsQuery);
            if (userSettings.Entities.Count > 0)
            {
                return (int)userSettings.Entities[0]["uilanguageid"];
            }
            return 0;
        }
    }
}
