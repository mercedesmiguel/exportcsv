﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xrm.Sdk;
using Config = ISE.SDK.Configuration;
using ISE.SDK.Extensions;
using ISE.SDK.MultiLang;
using Microsoft.Xrm.Sdk.Client;
using ISE.SDK.Plugin;

namespace ISE.SDK.Copy
{
    using IdMapping = Dictionary<Guid, EntityReference>;
    using RelationshipMapping = Dictionary<string, Dictionary<Guid, EntityReference>>;

    public static class EntityCopier
    {

        /// <summary>
        /// Copies the content of an entity into another entity using an entity map.
        /// </summary>
        /// <typeparam name="TTargetEntity">Type of the target entity</typeparam>
        /// <typeparam name="TSourceEntity">Type of the source entity</typeparam>
        /// <param name="source">Entity to be copied.</param>
        /// <param name="entityMap">Map to use to copy the entity.</param>
        /// <param name="pluginContext">The local plugin context. This is used to query dynamics crm.</param>
        /// <returns>A newly created target entity.</returns>
        public static TTargetEntity CopyTo<TTargetEntity, TPluginEntity>(this Entity source, Config.EntityMap entityMap, LocalPluginContext<TPluginEntity, OrganizationServiceContext> pluginContext)
            where TTargetEntity : Entity
            where TPluginEntity : Entity, new()
        {
            if (entityMap == null) throw new ArgumentNullException("entityMap");
            Entity target = new Entity(entityMap.TargetEntityLogicalName);
            target.Id = Guid.NewGuid();
            source.CopyTo(ref target, entityMap, pluginContext);
            return target.ToEntity<TTargetEntity>();
        }

        /// <summary>
        /// Copies the content of an entity into another entity using an entity map.
        /// </summary>
        /// <param name="source">Entity to be copied.</param>
        /// <param name="target">Reference to the target entity to which to write.</param>
        /// <param name="entityMap">Map to use to copy the entity.</param>
        /// <param name="pluginContext">The local plugin context. This is used to query dynamics crm.</param>
        public static void CopyTo<TTargetEntity, TPluginEntity>(this Entity source, ref TTargetEntity target, Config.EntityMap entityMap, LocalPluginContext<TPluginEntity, OrganizationServiceContext> pluginContext)
            where TTargetEntity : Entity
            where TPluginEntity : Entity
        {
            source.CopyTo(ref target, entityMap, pluginContext.TracingService);
        }

        /// <summary>
        /// Copies the content of an entity into another entity using an entity map.
        /// </summary>
        /// <param name="source">Entity to be copied.</param>
        /// <param name="target">Reference to the target entity to which to write.</param>
        /// <param name="entityMap">Map to use to copy the entity.</param>
        /// <param name="tracingService">The tracing service to log progress.</param>
        /// <param name="relationshipMapping">The relationship mapping for guids that need to be replaced</param>
        public static void CopyTo<TTargetEntity>(this Entity source, ref TTargetEntity target,
                                                 Config.EntityMap entityMap, ITracingService tracingService = null,
                                                 RelationshipMapping relationshipMapping = null)
            where TTargetEntity : Entity
        {
            if (target == null) throw new ArgumentNullException("target");
            if (entityMap == null) throw new ArgumentNullException("entityMap");

            // Check Type consistency
            if(tracingService != null) tracingService.Trace("Check Type consistency");

            if (!source.LogicalName.Equals(entityMap.SourceEntityLogicalName, StringComparison.OrdinalIgnoreCase))
            {
                throw new ArgumentException(String.Format("The entity map is for type '{0}' however the source is of type '{1}'", entityMap.SourceEntityLogicalName, source.LogicalName), "entityMap");
            }
            string targetTypeName = target.LogicalName;
            if (!targetTypeName.Equals(entityMap.TargetEntityLogicalName, StringComparison.OrdinalIgnoreCase))
            {
                throw new ArgumentException(String.Format("The entity map is for type '{0}' however the target is of type '{1}'", entityMap.TargetEntityLogicalName, targetTypeName), "entityMap");
            }

            foreach (Config.AttributeMap attributeMap in entityMap.AttributeMaps)
            {
                object sourceValue = null;

                if (source.Contains(attributeMap.SourceAttributeName) &&
                    source[attributeMap.SourceAttributeName] != null)
                {
                    sourceValue = source[attributeMap.SourceAttributeName];
                }

                SetTargetAttribute(source.LogicalName, attributeMap.SourceAttributeName, sourceValue, target,
                                   attributeMap.TargetAttributeName,
                                   attributeMap.DoNotWriteNull, tracingService);
            }

            if (entityMap.RelationshipDescriptions != null)
            {
                foreach (Config.RelationshipDescription relationshipDescription in entityMap.RelationshipDescriptions)
                {
                    if (relationshipMapping != null &&
                        relationshipMapping.ContainsKey(relationshipDescription.RelationshipName))
                    {
                        IdMapping mapping = relationshipMapping[relationshipDescription.RelationshipName];

                        if (source.Contains(relationshipDescription.SourceFieldName))
                        {
                            EntityReference sourceReference =
                                source[relationshipDescription.SourceFieldName] as EntityReference;

                            if (mapping != null && sourceReference != null && mapping.ContainsKey(sourceReference.Id))
                            {
                                object sourceValue =
                                    mapping[sourceReference.Id];

                                SetTargetAttribute(source.LogicalName, relationshipDescription.SourceFieldName,
                                                   sourceValue,
                                                   target,
                                                   relationshipDescription.TargetFieldName, true, tracingService);
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Generic function to set a value on the target entity
        /// </summary>
        /// <param name="sourceEntityLogicalName">The logical name of the source entity</param>
        /// <param name="sourceAttributeName">The name of the source attribute</param>
        /// <param name="sourceValue">The value of the source attribute</param>
        /// <param name="target">the target entity</param>
        /// <param name="targetAttributeName">the name of the target attribute</param>
        /// <param name="doNotWriteNull">should null values be written to the target?</param>
        /// <param name="tracingService">the tracing service for error tracing</param>
        private static void SetTargetAttribute(string sourceEntityLogicalName, string sourceAttributeName,
                                               object sourceValue, Entity target,
                                               string targetAttributeName, bool doNotWriteNull,
                                               ITracingService tracingService = null)
        {
            // should write null value
            if (sourceValue == null && !doNotWriteNull)
            {
                if (tracingService != null) tracingService.Trace("{0}({1})[{2}] = null", target.LogicalName, target.Id, targetAttributeName);
                target.AddAttributeValue(targetAttributeName, null);
            }
                // should write real value
            else if (sourceValue != null)
            {
                object targetValue = sourceValue;
                Guid targetGuidValue;

                // Convert the primary id value into an entity reference on copy
                if (Guid.TryParse(targetValue.ToString(), out targetGuidValue) &&
                    string.Format("{0}id", sourceEntityLogicalName)
                          .Equals(sourceAttributeName))
                {
                    targetValue = new EntityReference(sourceEntityLogicalName, targetGuidValue);
                }

                if (tracingService != null)
                    tracingService.Trace("{0}({1})[{2}] = {3} from {4}[{5}]", target.LogicalName, target.Id,
                                         targetAttributeName, TransformToString(targetValue), sourceEntityLogicalName, sourceAttributeName);

                target[targetAttributeName] = targetValue;
            }
        }

        private static string TransformToString(object value)
        {
            string result = value.ToString();
            if (value.GetType().Equals(typeof(EntityReference)))
            {
                var reference = value as EntityReference;
                result = string.Format("ER[{0},{1},{2}]", reference.LogicalName, reference.Id.ToString(), reference.Name);
            }

            return result;
        }
    }
}
