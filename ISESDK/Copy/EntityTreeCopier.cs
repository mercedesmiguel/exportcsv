﻿using System.Collections.Generic;
using Microsoft.Xrm.Sdk;
using System.Linq;
using Microsoft.Xrm.Sdk.Client;
using Config = ISE.SDK.Configuration;
using ISE.SDK.Plugin;

namespace ISE.SDK.Copy
{
    /// <summary>
    /// Extension for the Entity class that allows a Deeo Copy of a tree of related entities.
    /// </summary>
    public static class EntityTreeCopier
    {
        /// <summary>
        /// Extension method on the class Entity that creates a deep copy of the entity itself 
        /// and all sub-entities that are configured in the passed configuration.
        /// </summary>
        /// <typeparam name="TTargetEntity">Type of the entity</typeparam>
        /// <typeparam name="TPluginEntity">Type of the plugin entity</typeparam>
        /// <param name="source">Source entity that is the root of the copy operation</param>
        /// <param name="entityTreeConfiguration">Configuration for the deep copy</param>
        /// <param name="pluginContext">the pluginContext</param>
        /// <returns></returns>
        public static TTargetEntity DeepCopyTree<TTargetEntity, TPluginEntity>(this Entity source, Config.EntityTreeConfiguration entityTreeConfiguration,
                                                    LocalPluginContext<TPluginEntity, OrganizationServiceContext> pluginContext)
            where TTargetEntity : Entity
            where TPluginEntity : Entity, new()
        {
            // Keep a dictionary of all newly created entities
            //Dictionary<Guid,Entity> referenceUpdateMapping = new Dictionary<Guid, Entity>();

            // Copy all sub entities
            var entities = source.DeepCopySubtree(entityTreeConfiguration, pluginContext);

            entities.Reverse();

            foreach (var entity in entities)
            {
                pluginContext.TracingService.Trace("AddObject {0}", entity.LogicalName, entity.Id);
                //foreach (var attribute in entity.Attributes)
                //{
                //    tracingService.Trace("Attribute {0} {1}", attribute.Key, attribute.Value is EntityReference ? ((EntityReference)attribute.Value).Id : attribute.Value);
                //}
                //orgservice.Create(entity);
                pluginContext.ServiceContext.AddObject(entity);
            }

            // Persist changes
            pluginContext.TracingService.Trace("Persist changes");
            pluginContext.ServiceContext.SaveChanges();

            // Return new tree
            return entities.OfType<TTargetEntity>().FirstOrDefault();
        }

        /// <summary>
        /// Recursive method to clone all sub entity objects of a given entity
        /// </summary>
        /// <param name="source">Source entity that is the root of the copy operation</param>
        /// <param name="entityTreeNodeConfiguration">Configuration for the deep copy</param>
        /// <param name="pluginContext">the pluginContext</param>
        /// <param name="referenceUpdateMapping">The mapping of old references to new references that is 
        ///                                      to be filled during creation.</param>
        /// <returns></returns>
        private static List<Entity> DeepCopySubtree<TPluginEntity>(this Entity source,
                                              Config.EntityTreeNodeConfiguration entityTreeNodeConfiguration,
                                              LocalPluginContext<TPluginEntity, OrganizationServiceContext> pluginContext)
            where TPluginEntity : Entity, new()
        {
            List<Entity> entities = new List<Entity>();
            // Copy root entity
            pluginContext.TracingService.Trace("Copy {0} {1}", source.LogicalName, source.Id);
            Entity subtreeRoot = source.CopyTo<Entity, TPluginEntity>(entityTreeNodeConfiguration.CopyInstructions, pluginContext);
            pluginContext.TracingService.Trace("Copy has id {0}", subtreeRoot.Id);

            // Create copies of subentities
            if (entityTreeNodeConfiguration.ChildEntities != null)
            {
                pluginContext.TracingService.Trace("Go through child entities");
                foreach (var subNodeConfig in entityTreeNodeConfiguration.ChildEntities)
                {
                    // Load Entity
                    var subNodes =
                        pluginContext.ServiceContext.CreateQuery(subNodeConfig.CopyInstructions.SourceEntityLogicalName)
                                      .Where(
                                          subEntity =>
                                          subEntity.GetAttributeValue<EntityReference>(
                                              subNodeConfig.RootEntityReferenceField).Id.Equals(source.Id)).ToList();

                    // Create subnode copy
                    foreach (var subNode in subNodes)
                    {
                        pluginContext.ServiceContext.Detach(subNode);
                        subNode[subNodeConfig.RootEntityReferenceField] = new EntityReference(subtreeRoot.LogicalName, subtreeRoot.Id);
                        pluginContext.TracingService.Trace("Set reference field {0} to {1}", subNodeConfig.RootEntityReferenceField, subtreeRoot.Id);
                        entities.AddRange(subNode.DeepCopySubtree(subNodeConfig, pluginContext));
                    }
                }
            }

            entities.Add(subtreeRoot);

            return entities;
        }
    }
}
