﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ISE.SDK.EntitySupport
{
    using Microsoft.Crm.Sdk.Messages;
    using Microsoft.Xrm.Sdk.Metadata;
    using Microsoft.Xrm.Sdk;
    using System.Globalization;
    using Microsoft.Xrm.Sdk.Messages;
    using Microsoft.Xrm.Sdk.Query;
    using Ise.SharePoint.DocumentIntegration.Xrm;

    internal static class PropertyHelper
    {
        private static readonly Dictionary<Guid, string> EntitySchemanameDisplayname = new Dictionary<Guid, string>();

        /// <summary>
        /// Returns corresponding string values of OptionsSets and EntityReferences
        /// </summary>
        /// <param name="attribute">The attribute where key is the name and value the crm object</param>
        /// <param name="entityMetadata">The entity metadata to the look for the string</param>
        /// <param name="organizationService">The organizationservice to query from if needed (only true for EntityReferences)</param>
        /// <param name="alternateAttribute"></param>
        /// <param name="timeZoneCode"></param>
        /// <returns></returns>
        public static object GetValueToString(KeyValuePair<string, object> attribute, EntityMetadata entityMetadata,
                                              IOrganizationService organizationService, string alternateAttribute, int timeZoneCode = -1)
        {
            if (attribute.Value == null)
                return string.Empty;

            if (attribute.Value is OptionSetValue)
            {
                OptionSetValue optionSetValue = attribute.Value as OptionSetValue;
                AttributeMetadata attributeMetadata =
                    entityMetadata.Attributes.FirstOrDefault(
                        metaAttribute =>
                        String.Compare(metaAttribute.SchemaName, attribute.Key, true, CultureInfo.InvariantCulture) ==
                        0);
                if (attributeMetadata is PicklistAttributeMetadata)
                {
                    PicklistAttributeMetadata picklistAttributeMetadata =
                        attributeMetadata as PicklistAttributeMetadata;

                    if (picklistAttributeMetadata.OptionSet.IsGlobal != null)
                    {
                        OptionMetadata optionSetValueMetadata = null;

                        // Determine whether the option set is global or entity specific
                        if (picklistAttributeMetadata.OptionSet.IsGlobal.Value)
                        {
                            RetrieveOptionSetRequest retrieveOptionSetRequest = new RetrieveOptionSetRequest()
                                {
                                    Name = picklistAttributeMetadata.OptionSet.Name
                                };

                            RetrieveOptionSetResponse retrieveOptionSetResponse =
                                (RetrieveOptionSetResponse) organizationService.Execute(retrieveOptionSetRequest);
                            OptionSetMetadata globalOptionSet =
                                retrieveOptionSetResponse.OptionSetMetadata as OptionSetMetadata;

                            if (globalOptionSet != null)
                            {
                                optionSetValueMetadata =
                                    globalOptionSet.Options.FirstOrDefault(
                                        option => option.Value.HasValue && option.Value == optionSetValue.Value);
                            }
                        }
                        else
                        {
                            optionSetValueMetadata = picklistAttributeMetadata.OptionSet.Options
                                                                              .FirstOrDefault(
                                                                                  option =>
                                                                                  option.Value.HasValue &&
                                                                                  option.Value ==
                                                                                  optionSetValue.Value);
                        }

                        if (optionSetValueMetadata != null)
                        {
                            return optionSetValueMetadata.Label.UserLocalizedLabel.Label;
                        }
                    }
                }
                return string.Empty;
            }

            if (attribute.Value is Money)
            {
                Money moneyValue = attribute.Value as Money;
                if (moneyValue != null)
                    return moneyValue.Value;
                else
                    return 0;
            }

            if (attribute.Value is DateTime)
            {
                DateTime dateTime = (DateTime) attribute.Value;

                if (timeZoneCode > -1)
                {
                    var request = new LocalTimeFromUtcTimeRequest
                        {
                            TimeZoneCode = timeZoneCode,
                            UtcTime = dateTime.ToUniversalTime()
                        };

                    var response = (LocalTimeFromUtcTimeResponse) organizationService.Execute(request);

                    dateTime = response.LocalTime;
                }

                return dateTime;
            }

            if (attribute.Value is EntityReference)
            {
                EntityReference entityReference = attribute.Value as EntityReference;

                if (EntitySchemanameDisplayname.ContainsKey(entityReference.Id))
                {
                    return EntitySchemanameDisplayname[entityReference.Id];
                }

                RetrieveEntityRequest retrieveEntityRequest = new RetrieveEntityRequest();
                retrieveEntityRequest.EntityFilters = EntityFilters.Entity;
                retrieveEntityRequest.LogicalName = entityReference.LogicalName;

                RetrieveEntityResponse retrieveEntityResponse =
                    (RetrieveEntityResponse) organizationService.Execute(retrieveEntityRequest);
                if (retrieveEntityResponse.EntityMetadata != null)
                {
                    // If the last part of the source attribute name chain differs from the attribute name it
                    // is considererd an alternate attribute that has to be retrieved from the related entity.
                    string attributeName = (attribute.Key == alternateAttribute
                                                ? retrieveEntityResponse.EntityMetadata.PrimaryNameAttribute
                                                : alternateAttribute);

                    // Query the referenced entity for the needed attribute
                    RetrieveRequest retrieveRequest = new RetrieveRequest()
                        {
                            ColumnSet = new ColumnSet(attributeName),
                            Target = entityReference
                        };

                    // Execute the query
                    RetrieveResponse retrieveResponse =
                        (RetrieveResponse) organizationService.Execute(retrieveRequest);

                    // If the needed attribute is filled return its value
                    if (retrieveResponse.Entity.Contains(attributeName))
                    {
                        string retrieveValue = retrieveResponse.Entity[attributeName].ToString();
                        EntitySchemanameDisplayname.Add(entityReference.Id, retrieveValue);
                        return retrieveValue;
                    }
                }

                return string.Empty;
            }


            return attribute.Value;
        }

        /// <summary>
        /// This helper method returns the metadata for the given entity
        /// </summary>
        /// <param name="entityName"></param>
        /// <param name="organizationService"></param>
        /// <returns></returns>
        public static EntityMetadata LoadMetadata(string entityName, IOrganizationService organizationService)
        {
            RetrieveEntityRequest retrieveEntityRequest = new RetrieveEntityRequest();
            retrieveEntityRequest.EntityFilters = EntityFilters.Attributes | EntityFilters.Relationships;
            retrieveEntityRequest.LogicalName = entityName;

            RetrieveEntityResponse retrieveEntityResponse = (RetrieveEntityResponse) organizationService.Execute(retrieveEntityRequest);
            
            return retrieveEntityResponse.EntityMetadata;
        }

        /// <summary>
        /// Returns the label for a given attribute from the metadata
        /// </summary>
        /// <param name="attributeName"></param>
        /// <param name="metadata"></param>
        /// <returns></returns>
        public static string GetAttributeLabelFromMetadata(string attributeName, EntityMetadata metadata)
        {
            string attributeLabel = "UNKOWN";
            var attribute =
                metadata.Attributes.FirstOrDefault(
                    attributeMetadata => attributeMetadata.LogicalName.Equals(attributeName));

            if (attribute != null)
                attributeLabel = attribute.DisplayName.UserLocalizedLabel.Label;

            return attributeLabel;
        }


        public static int GetUserTimeZoneCode(Guid userId, IOrganizationService organizationService)
        {
            int timeZoneCode = -1;

            QueryExpression userSettingQuery = new QueryExpression(UserSettings.EntityLogicalName);
            userSettingQuery.Criteria.AddCondition("systemuserid", ConditionOperator.Equal, userId);
            userSettingQuery.ColumnSet = new ColumnSet("timezonecode");

            EntityCollection userSettings = organizationService.RetrieveMultiple(userSettingQuery);

            if (userSettings.Entities.Count > 0)
            {
                timeZoneCode = userSettings.Entities.First().ToEntity<UserSettings>().TimeZoneCode ?? -1;
            }

            return timeZoneCode;
        }
    }
}
