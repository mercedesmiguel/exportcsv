﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xrm.Sdk;

namespace ISE.SDK.Extensions
{
    public static class EntityExtensions
    {
        public static void AddAttributeValue(this Entity entity, KeyValuePair<string, object> attribute)
        {
            entity.AddAttributeValue(attribute.Key, attribute.Value);
        }

        public static void AddAttributeValue(this Entity entity, string key, object value)
        {
            if (entity.Contains(key))
            {
                entity[key] = value;
            }
            else
            {
                entity.Attributes.Add(key, value);
            }
        }

        /// <summary>
        /// Returns the attribute value, or null, if the attribute does not exist.
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="attributeName"></param>
        /// <returns></returns>
        public static object GetAttributeValue(this Entity entity, string attributeName)
        {
            return entity.Contains(attributeName) ? entity[attributeName] : null;
        }
    }
}
