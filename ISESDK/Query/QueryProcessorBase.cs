﻿using System;
using System.Collections.Generic;
using Microsoft.Xrm.Sdk;

namespace ISE.SDK.Query
{
    /// <summary>
    /// Common base class for all query processors
    /// </summary>
    public abstract class QueryProcessorBase : IQueryProcessor, IDisposable
    {
        /// <summary>
        /// The organization service for the query Operation
        /// </summary>
        protected readonly IOrganizationService _organizationService;

        /// <summary>
        /// Number of records that should be queried at once
        /// </summary>
        protected readonly int _pageSize;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="organizationService">the organization service</param>
        /// <param name="pageSize">Number of records fetched at once</param>
        protected QueryProcessorBase(IOrganizationService organizationService, int pageSize = 5000)
        {
            _organizationService = organizationService;
            _pageSize = pageSize;
        }

        /// <summary>
        /// To be implemented in subclasses
        /// </summary>
        /// <returns>the list of retrieved entities</returns>
        public abstract List<Entity> ProcessQuery();

        public void Dispose()
        {
            // nothing to do here
        }
    }
}
