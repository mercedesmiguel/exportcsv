﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;

namespace ISE.SDK.Query
{
    public class FetchExpressionProcessor : QueryProcessorBase
    {
        /// <summary>
        /// Expression to be executed.
        /// </summary>
        private readonly string _fetchXml;

        /// <summary>
        /// Action for processing the entity
        /// </summary>
        private readonly Action<Entity> _processEntity;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="organizationService">the organization service</param>
        /// <param name="fetchXml">The fetch expression to be processed</param>
        /// <param name="processEntity">Operation for processing an entity</param>
        /// <param name="pageSize">The number of records per page</param>
        public FetchExpressionProcessor(IOrganizationService organizationService, string fetchXml, Action<Entity> processEntity, int pageSize = 5000)
            : base(organizationService, pageSize)
        {
            _fetchXml = fetchXml;
            _processEntity = processEntity;
        }

        /// <summary>
        /// Implementation of the query processing
        /// </summary>
        /// <returns></returns>
        public override List<Entity> ProcessQuery()
        {
            var result = new List<Entity>();

            // Use paging here as described in http://msdn.microsoft.com/en-us/library/cc151070.aspx
            string pagingCookie = null;// The number of records per page to retrieve.
            int pageNumber = 1;
            
            while (true)
            {
                // Build fetchXml string with the placeholders.
                string xml = CreateXml(_fetchXml, pagingCookie, pageNumber, _pageSize);

                EntityCollection entities = _organizationService.RetrieveMultiple(new FetchExpression(xml));

                foreach (var entity in entities.Entities)
                {
                    _processEntity(entity);
                }
                result.AddRange(entities.Entities);

                // Check for morerecords, if it returns 1.
                if (entities.MoreRecords)
                {
                    // Increment the page number to retrieve the next page.
                    pageNumber++;
                    pagingCookie = entities.PagingCookie;
                }
                else
                {
                    // If no more records in the result nodes, exit the loop.
                    break;
                }

            }

            return result;
        }

        static string CreateXml(string xml, string cookie, int page, int count)
        {
            StringReader stringReader = new StringReader(xml);
            XmlTextReader reader = new XmlTextReader(stringReader);

            // Load document
            XmlDocument doc = new XmlDocument();
            doc.Load(reader);

            return CreateXml(doc, cookie, page, count);
        }

        static string CreateXml(XmlDocument doc, string cookie, int page, int count)
        {
            XmlAttributeCollection attrs = doc.DocumentElement.Attributes;

            if (cookie != null)
            {
                XmlAttribute pagingAttr = doc.CreateAttribute("paging-cookie");
                pagingAttr.Value = cookie;
                attrs.Append(pagingAttr);
            }

            XmlAttribute pageAttr = doc.CreateAttribute("page");
            pageAttr.Value = Convert.ToString(page);
            attrs.Append(pageAttr);

            XmlAttribute countAttr = doc.CreateAttribute("count");
            countAttr.Value = Convert.ToString(count);
            attrs.Append(countAttr);

            StringBuilder sb = new StringBuilder(1024);
            StringWriter stringWriter = new StringWriter(sb);

            XmlTextWriter writer = new XmlTextWriter(stringWriter);
            doc.WriteTo(writer);
            writer.Close();

            return sb.ToString();
        }
    }
}
