﻿using System.Collections.Generic;
using Microsoft.Xrm.Sdk;

namespace ISE.SDK.Query
{
    /// <summary>
    /// Common interface for different types of query operations available in CRM.
    /// </summary>
    public interface IQueryProcessor
    {
        /// <summary>
        /// Executes the query and returns a list of entities
        /// </summary>
        /// <returns></returns>
        List<Entity> ProcessQuery();
    }
}
