﻿using System;
using System.Collections.Generic;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;

namespace ISE.SDK.Query
{
    public class QueryExpressionProcessor : QueryProcessorBase
    {
        /// <summary>
        /// Expression to be executed.
        /// </summary>
        private readonly QueryExpression _queryExpression;

        /// <summary>
        /// Action for processing the entity
        /// </summary>
        private readonly Action<Entity> _processEntity;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="organizationService">the organization service</param>
        /// <param name="queryExpression">The query expression to be processed</param>
        /// <param name="processEntity">Operation for processing an entity</param>
        /// <param name="pageSize">The number of records per page</param>
        public QueryExpressionProcessor(IOrganizationService organizationService, QueryExpression queryExpression, Action<Entity> processEntity, int pageSize = 5000)
            : base(organizationService, pageSize)
        {
            _queryExpression = queryExpression;
            _processEntity = processEntity;
        }

        /// <summary>
        /// Implementation of the query processing
        /// </summary>
        /// <returns></returns>
        public override List<Entity> ProcessQuery()
        {
            var result = new List<Entity>();

            // Use paging here as described in http://msdn.microsoft.com/en-us/library/gg327917.aspx
            _queryExpression.PageInfo = new PagingInfo {PageNumber = 1, Count = _pageSize, PagingCookie = null };

            while (true)
            {
                EntityCollection entities = _organizationService.RetrieveMultiple(_queryExpression);

                if (entities.Entities != null)
                {
                        foreach (var entity in entities.Entities)
                        {
                            _processEntity(entity);
                            result.Add(entity);
                        }
                }

                if (entities.MoreRecords)
                {
                    _queryExpression.PageInfo.PageNumber++;
                    _queryExpression.PageInfo.PagingCookie = entities.PagingCookie;
                }
                else
                {
                    // If no more records are in the result nodes, exit the loop.
                    break;
                }
            }

            return result;
        }
    }
}
