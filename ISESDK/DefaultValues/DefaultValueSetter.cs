﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ISE.SDK.Configuration;
using ISE.SDK.Extensions;
using Microsoft.Xrm.Sdk;

namespace ISE.SDK.DefaultValues
{
    public static class DefaultValueSetter
    {
        public static void SetDefaultValue(this Entity entity, DefaultAttributeValue defaultAttributeValue)
        {
            switch (defaultAttributeValue.Type)
            {
                case AttributeType.String:
                    entity.AddAttributeValue(defaultAttributeValue.AttributeName, defaultAttributeValue.Value);
                    break;
                case AttributeType.Int:
                    entity.AddAttributeValue(defaultAttributeValue.AttributeName, int.Parse(defaultAttributeValue.Value));
                    break;
                case AttributeType.Money:
                    entity.AddAttributeValue(defaultAttributeValue.AttributeName, new Money(decimal.Parse(defaultAttributeValue.Value)));
                    break;
                case AttributeType.Lookup:
                    throw new NotSupportedException("Default values of type 'Lookup' are not supported.");
                    break;
                case AttributeType.OptionSet:
                    entity.AddAttributeValue(defaultAttributeValue.AttributeName, new OptionSetValue(int.Parse(defaultAttributeValue.Value)));
                    break;
                case AttributeType.Bool:
                    entity.AddAttributeValue(defaultAttributeValue.AttributeName, bool.Parse(defaultAttributeValue.Value));
                    break;
                case AttributeType.Double:
                    entity.AddAttributeValue(defaultAttributeValue.AttributeName, double.Parse(defaultAttributeValue.Value));
                    break;
                case AttributeType.Decimal:
                    entity.AddAttributeValue(defaultAttributeValue.AttributeName, decimal.Parse(defaultAttributeValue.Value));
                    break;
                default:
                    throw new ArgumentOutOfRangeException("defaultAttributeValue");
            }
        }
    }
}
