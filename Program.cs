﻿using System.Configuration;
using ISE.SDK.Tools;
using System;


namespace CenteraExporter
{
    public class Program
    {
        static void Main(string[] args)
        {
            try
            {
                CrmConnector crmConnection = new CrmConnector();
                crmConnection.OpenFromFile("config.xml");
                Console.WriteLine(crmConnection.OrganizationService != null ? "Connection erfolgreich erstellt": "Connection NO erfolgreich erstellt");

                CrmConnectionHandler connectionHandler = new CrmConnectionHandler(crmConnection);
                connectionHandler.Execution("FetchXml.xml");

                Console.WriteLine();
                Console.WriteLine("Done. Press any key to exit.");
                Console.Read();
            }
            catch (Exception e)
            {
                Console.WriteLine("Error: " + e.ToString());
                Console.WriteLine("BaseException: " + e.GetBaseException());

                Console.WriteLine();
                Console.WriteLine("Done. Press any key to exit.");
                Console.Read();
            }





            #region FPPool
            //FPPool thePool = new FPPool(ConfigFactory.WrapProdConfiguration("ginswfh").CenteraCluster);
            //FPClip clipRef = new FPClip(thePool, "6F568C093USIAe462ETMDNVTQ6K", FPMisc.OPEN_ASTREE);
            ////clipRef.Attributes
            //// Iterate across the Tag (and their Attr) collections
            //foreach (FPTag tg in clipRef.Tags)
            //{
            //    if (tg.Name == "CRMUploadedTag")
            //    {
            //        foreach (FPAttribute a in tg.Attributes)
            //        {
            //            Console.WriteLine(a.Name + ": " + a.Value);
            //            //if (a.Name == "ContentType") info.FileType = a.Value;
            //            //if (a.Name == "Filename") info.FileName = a.Value;
            //            //if (a.Name == "Shortname") info.ShortFilename = a.Value;
            //        }
            //    }
            //}
            #endregion
        }
    }
}
