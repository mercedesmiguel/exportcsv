﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using ISE.SDK.Tools;
using System.IO;
using System;
using ISE.SDK.Query;
using System.Collections.Generic;
using System.Text;
using CenteraExporter.Classes;

namespace CenteraExporter
{
    public class CrmConnectionHandler
    {
        private CrmConnector _connector;
        private string _lastcompanyname = string.Empty;

        private OrganizationServiceContext OrganizationServiceContext
        {
            get { return _connector.OrganizationServiceContext; }
        }
        private IOrganizationService OrganizationService
        {
            get { return _connector.OrganizationService; }
        }
        public CrmConnectionHandler(CrmConnector crmConnector)
        {
            _connector = crmConnector;
        }

        private List<String> csvProperties = new List<string>();

        private const String pathCSV = "C:\\Repoexportcsv\\ExportCSV\\Entities.csv";

        public void Execution(string copyConfigFile)
        {
            String fetchXml = String.Empty;

            if (!String.IsNullOrEmpty(copyConfigFile))
            {
                //Readl config from file (config.xml)
                var reader = new StreamReader(new FileStream(copyConfigFile, FileMode.Open));
                try
                {
                    fetchXml = reader.ReadToEnd();
                }
                finally
                {
                    reader.Close();
                }

                Console.WriteLine("OrganizationService: " + OrganizationService.ToString());

                //Write header CSV
                StringBuilder csv = new StringBuilder();
                StringBuilder csvtempheader = new StringBuilder();
                StringBuilder csvtemp = new StringBuilder();
                //TODO
                //csv.AppendLine(GetHeader().ToString());
                csv.AppendLine(HeaderFirma(csvtempheader).ToString());

                //IQueryProcessor processor{}
                //ent => WriteEntityDataInCsv(ent, csv, csvtemp)

                var processor = new FetchExpressionProcessor(OrganizationService, fetchXml, x => { }, 1000);
                List<Entity> listElems = processor.ProcessQuery();

                int countAnspr = 0;
                Entity vorherigenEntity = null;
                foreach(Entity entity in listElems)
                {
                    //csvtemp = WriteEntityDataInCsv(entity, csv, csvtemp);

                    if(vorherigenEntity == null){
                        vorherigenEntity = entity;
                    }

                    if (String.IsNullOrEmpty(_lastcompanyname) || _lastcompanyname != entity.Attributes["companyname"].ToString())
                    {
                        if (!String.IsNullOrEmpty(csvtemp.ToString()))
                        {
                            while (countAnspr <= 4)
                            {
                                csvtemp = DataAnsprechpartner(new Entity(), csvtemp);
                                countAnspr++;
                            }
                            csvtemp = DataFirma2(vorherigenEntity, csvtemp);
                            csv.AppendLine(csvtemp.ToString());
                            vorherigenEntity = entity;
                        }

                        csvtemp = new StringBuilder();
                        countAnspr = 0;

                        //DATA FROM FIRMA (LEAD)
                        csvtemp = DataFirma1(entity, csvtemp);
                        //DATA FROM ANSPRECHPARTNER
                        csvtemp = DataAnsprechpartner(entity, csvtemp);
                        countAnspr++;
                    }
                    else if (!String.IsNullOrEmpty(_lastcompanyname) && _lastcompanyname == entity.Attributes["companyname"].ToString())
                    {
                        if (countAnspr <= 4)
                        {
                            //DATA FROM ANSPRECHPARTNER
                            csvtemp = DataAnsprechpartner(entity, csvtemp);
                            countAnspr++;
                        }
                    }

                    _lastcompanyname = entity.Attributes["companyname"].ToString();
                }

                csv.AppendLine(csvtemp.ToString());
                WriteValuesCsv(csv);
            }
        }

        private string WriteDataCSV(Entity entity, String attribname)
        {
            string datatocsv = string.Empty;
            if (entity.Attributes.Contains(attribname))
            {
                datatocsv = entity.Attributes[attribname].ToString() + ";";
            }
            else
            {
                datatocsv = ";";
            }

            return datatocsv;
        }
        private string WriteDataOptionsetCSV(Entity entity, String attribname)
        {
            string datatocsv = string.Empty;
            if (entity.Attributes.Contains(attribname))
            {
                datatocsv = ((Microsoft.Xrm.Sdk.OptionSetValue)entity.Attributes[attribname]).Value + ";";
            }
            else
            {
                datatocsv = ";";
            }

            return datatocsv;
        }
        private StringBuilder DataFirma1(Entity entity, StringBuilder csvtemp)
        {
            //Anzahl Auftraggeber
            csvtemp.Append(WriteDataCSV(entity, "anzahlauftraggeber")); //TODO
            //Firma1
            csvtemp.Append(WriteDataCSV(entity, "companyname"));
            //Firma2
            csvtemp.Append(WriteDataCSV(entity, "companyname2")); //TODO
            //Strasse
            csvtemp.Append(WriteDataCSV(entity, "adress1_line1"));
            //PLZ
            csvtemp.Append(WriteDataCSV(entity, "address1_postalcode"));
            //Ort
            csvtemp.Append(WriteDataCSV(entity, "address1_city"));
            //Telefon
            csvtemp.Append(WriteDataCSV(entity, "address1_telephone1"));
            //Telefax
            csvtemp.Append(WriteDataCSV(entity, "address1_fax"));
            //Info_eMail
            csvtemp.Append(WriteDataCSV(entity, "emailaddress1"));
            //Website
            csvtemp.Append(WriteDataCSV(entity, "websiteurl"));
            //Land
            csvtemp.Append(WriteDataCSV(entity, "address1_stateorprovince"));

            //Konzernstatus
            csvtemp.Append(WriteDataCSV(entity, "Konzernstatus")); //TODO
            //Konzername
            csvtemp.Append(WriteDataCSV(entity, "Konzername")); //TODO
            //Konzernort
            csvtemp.Append(WriteDataCSV(entity, "Konzernort")); //TODO
            //Konzernland
            csvtemp.Append(WriteDataCSV(entity, "Konzernland")); //TODO


            return csvtemp;
        }
        private StringBuilder DataAnsprechpartner(Entity entity, StringBuilder csvtemp)
        {
            //Anrede
            csvtemp.Append(WriteDataCSV(entity, "salutation"));
            //Titel
            csvtemp.Append(WriteDataCSV(entity, "ise_suffix")); 
            //Vorname
            csvtemp.Append(WriteDataCSV(entity, "firstname"));
            //Nachname
            csvtemp.Append(WriteDataCSV(entity, "lastname"));
            //Funktionstyp
            csvtemp.Append(WriteDataCSV(entity, "Funktionstyp")); //TODO
            //Funcktionsname
            csvtemp.Append(WriteDataCSV(entity, "Funcktionsname")); //TODO
            //Abteilung
            csvtemp.Append(WriteDataCSV(entity, "jobtitle")); //TODO ????
            //DW
            csvtemp.Append(WriteDataCSV(entity, "DW")); //TODO
            //Mobil
            csvtemp.Append(WriteDataCSV(entity, "mobilephone"));
            //Email
            csvtemp.Append(WriteDataCSV(entity, "emailaddress1"));
            //OP in out
            csvtemp.Append(WriteDataCSV(entity, "opinout")); //TODO
            //newsletter
            csvtemp.Append(WriteDataCSV(entity, "newsletter")); //TODO
            //Veranstaltung
            csvtemp.Append(WriteDataCSV(entity, "Veranstaltung"));  //TODO

            return csvtemp;
        }
        private StringBuilder DataFirma2(Entity entity, StringBuilder csvtemp)
        {
            //074_Anzahl MA
            csvtemp.Append(WriteDataCSV(entity, "numberofemployees"));
            //075_Kategorie MA
            csvtemp.Append(WriteDataCSV(entity, "kategorieMA")); //TODO
            //184_Anzahl_Standorte
            csvtemp.Append(WriteDataCSV(entity, "184_Anzahl_Standorte")); //TODO
            //080_Branchenbezeichnung
            csvtemp.Append(WriteDataCSV(entity, "ise_brancheleadquelle"));
            //194_ITK Profil 1
            csvtemp.Append(WriteDataCSV(entity, "itkprofil1"));//TODO
            //095_ITK Profil 2
            csvtemp.Append(WriteDataCSV(entity, "itkprofil2"));//TODO
            //093_ITK Profil 3
            csvtemp.Append(WriteDataCSV(entity, "itkprofil3"));//TODO
            //094_ITK Profil 4
            csvtemp.Append(WriteDataCSV(entity, "itkprofil4"));//TODO
            //150_Letzter_Kontakt_Kunde
            csvtemp.Append(WriteDataCSV(entity, "letzter_Kontakt_Kunde"));//TODO
            //WIEDERVORLAGE_Kunde
            csvtemp.Append(WriteDataCSV(entity, "WIEDERVORLAGE_Kunde"));//TODO
            //Status
            csvtemp.Append(WriteDataOptionsetCSV(entity, "statuscode"));//TODO
            //Historie
            csvtemp.Append(WriteDataCSV(entity, "historie"));//TODO

            return csvtemp;
        }

        private StringBuilder HeaderFirma(StringBuilder csvtemp)
        {
            csvtemp.Append("Kontakt Nr. Auftraggeber" + ";");
            csvtemp.Append("009_Firma_1" + ";");
            csvtemp.Append("010_Firma_2" + ";");
            csvtemp.Append("011_Strasse" + ";");
            csvtemp.Append("012_PLZ" + ";");
            csvtemp.Append("013_Ort" + ";");
            csvtemp.Append("016_Telefon" + ";");
            csvtemp.Append("017_Telefax" + ";");

            csvtemp.Append("018_Info_eMail" + ";");
            csvtemp.Append("019_WebSite" + ";");
            csvtemp.Append("014_Land" + ";");
            csvtemp.Append("020_Konzernstatus" + ";");
            csvtemp.Append("021_Konzernname" + ";");
            csvtemp.Append("022_Konzernort" + ";");
            csvtemp.Append("023_Konzernland" + ";");

            //AP1
            csvtemp.Append("024_AP_1_Anrede" + ";");
            csvtemp.Append("025_AP_1_Titel" + ";");
            csvtemp.Append("026_AP_1_Vorname" + ";");
            csvtemp.Append("027_AP_1_Nachname" + ";");
            csvtemp.Append("028_AP_1_Funktionstyp" + ";");
            csvtemp.Append("029_AP_1_Funktionsname" + ";");
            csvtemp.Append("030_AP_1_Abteilung" + ";");
            csvtemp.Append("031_AP_1_DW" + ";");
            csvtemp.Append("032_AP_1_Mobil" + ";");
            csvtemp.Append("033_AP_1_eMail" + ";");
            csvtemp.Append("293_AP_1_OPT_in_out" + ";");
            csvtemp.Append("294_AP_1_Newsletter" + ";");
            csvtemp.Append("295_AP_1_Veranstaltung" + ";");

            //AP2
            csvtemp.Append("034_AP_2_Anrede" + ";");
            csvtemp.Append("035_AP_2_Titel" + ";");
            csvtemp.Append("036_AP_2_Vorname" + ";");
            csvtemp.Append("037_AP_2_Nachname" + ";");
            csvtemp.Append("038_AP_2_Funktionstyp" + ";");
            csvtemp.Append("039_AP_2_Funktionsname" + ";");
            csvtemp.Append("040_AP_2_Abteilung" + ";");
            csvtemp.Append("041_AP_2_DW" + ";");
            csvtemp.Append("042_AP_2_Mobil" + ";");
            csvtemp.Append("043_AP_2_eMail" + ";");
            csvtemp.Append("297_AP_2_OPT_in_out" + ";");
            csvtemp.Append("298_AP_2_Newsletter" + ";");
            csvtemp.Append("299_AP_2_Veranstaltung" + ";");

            //AP3
            csvtemp.Append("044_AP_3_Anrede" + ";");
            csvtemp.Append("045_AP_3_Titel" + ";");
            csvtemp.Append("046_AP_3_Vorname" + ";");
            csvtemp.Append("047_AP_3_Nachname" + ";");
            csvtemp.Append("048_AP_3_Funktionstyp" + ";");
            csvtemp.Append("049_AP_3_Funktionsname" + ";");
            csvtemp.Append("050_AP_3_Abteilung" + ";");
            csvtemp.Append("051_AP_3_DW" + ";");
            csvtemp.Append("052_AP_3_Mobil" + ";");
            csvtemp.Append("053_AP_3_eMail" + ";");
            csvtemp.Append("301_AP_3_OPT_in_out" + ";");
            csvtemp.Append("302_AP_3_Newsletter" + ";");
            csvtemp.Append("303_AP_3_Veranstaltung" + ";");

            //AP4
            csvtemp.Append("054_AP_4_Anrede" + ";");
            csvtemp.Append("055_AP_4_Titel" + ";");
            csvtemp.Append("056_AP_4_Vorname" + ";");
            csvtemp.Append("057_AP_4_Nachname" + ";");
            csvtemp.Append("058_AP_4_Funktionstyp" + ";");
            csvtemp.Append("059_AP_4_Funktionsname" + ";");
            csvtemp.Append("060_AP_4_Abteilung" + ";");
            csvtemp.Append("061_AP_4_DW" + ";");
            csvtemp.Append("062_AP_4_Mobil" + ";");
            csvtemp.Append("063_AP_4_eMail" + ";");
            csvtemp.Append("305_AP_4_OPT_in_out" + ";");
            csvtemp.Append("306_AP_4_Newsletter" + ";");
            csvtemp.Append("307_AP_4_Veranstaltung" + ";");

            //AP5
            csvtemp.Append("064_AP_5_Anrede" + ";");
            csvtemp.Append("065_AP_5_Titel" + ";");
            csvtemp.Append("066_AP_5_Vorname" + ";");
            csvtemp.Append("067_AP_5_Nachname" + ";");
            csvtemp.Append("068_AP_5_Funktionstyp" + ";");
            csvtemp.Append("069_AP_5_Funktionsname" + ";");
            csvtemp.Append("070_AP_5_Abteilung" + ";");
            csvtemp.Append("071_AP_5_DW" + ";");
            csvtemp.Append("072_AP_5_Mobil" + ";");
            csvtemp.Append("073_AP_5_eMail" + ";");
            csvtemp.Append("309_AP_5_OPT_in_out" + ";");
            csvtemp.Append("310_AP_5_Newsletter" + ";");
            csvtemp.Append("311_AP_5_Veranstaltung" + ";");

            //Rest data Lead
            csvtemp.Append("074_Anzahl MA" + ";");
            csvtemp.Append("075_Kategorie MA" + ";");
 	        csvtemp.Append("184_Anzahl_Standorte" + ";");
            csvtemp.Append("080_Branchenbezeichnung" + ";");
            csvtemp.Append("194_ITK Profil 1" + ";");
            csvtemp.Append("095_ITK Profil 2" + ";");
            csvtemp.Append("093_ITK Profil 3" + ";");
            csvtemp.Append("094_ITK Profil 4" + ";");
            csvtemp.Append("150_Letzter_Kontakt_Kunde" + ";");
            csvtemp.Append("WIEDERVORLAGE_Kunde" + ";");
            csvtemp.Append("STATUS" + ";");
            csvtemp.Append("Historie" + ";");

            return csvtemp;

        }



        private void WriteValuesCsv(StringBuilder totalTextCsv)
        {
            Console.WriteLine("CSV wird geschrieben...");

            Encoding dosEncoding = Encoding.GetEncoding(437);
            byte[] encBytes = dosEncoding.GetBytes(totalTextCsv.ToString());
            byte[] utf8Bytes = Encoding.Convert(dosEncoding, Encoding.Default, encBytes);

            string converted = Encoding.Default.GetString(utf8Bytes);
            File.AppendAllText(pathCSV, converted);
        }

        private StringBuilder WriteEntityDataInCsv(Entity entity, StringBuilder csv, StringBuilder csvtemp)
        {
            if (String.IsNullOrEmpty(_lastcompanyname) || _lastcompanyname != entity.Attributes["companyname"].ToString())
            {
                csv.AppendLine(csvtemp.ToString());
                csvtemp = new StringBuilder();

                csvtemp.Append(GetData(entity));
                csvtemp.Append(GetDataAnsprechpartner(entity));
            }
            else if (!String.IsNullOrEmpty(_lastcompanyname) && _lastcompanyname == entity.Attributes["companyname"].ToString())
            {
                csvtemp.Append(GetDataAnsprechpartner(entity));
            }

            _lastcompanyname = entity.Attributes["companyname"].ToString();

            return csvtemp;
        }

        private StringBuilder GetHeader()
        {
            var header = new StringBuilder();
            PropertyIterator<CrmName, DokumentInfo>.Iterate((attr, prop) =>
            {
                if (!String.IsNullOrEmpty(header.ToString()))
                {
                    header.Append(";");
                }
                header.Append(attr.Name);
            });

            PropertyIterator<CrmNameAnspr, DokumentInfo>.Iterate((attr, prop) =>
            {
                if (!String.IsNullOrEmpty(header.ToString()))
                {
                    header.Append(";");
                }
                header.Append(attr.Name);
            });
            return header;
        }

        private String GetData(Entity crmEntity)
        {
            String csvLine = String.Empty;

            try
            {

                if (crmEntity != null)
                {
                    csvLine = GetAttr(crmEntity);
                }

            }
            finally
            {


            }

            return csvLine;
        }

        private String GetDataAnsprechpartner(Entity crmEntity)
        {
            String csvLine = String.Empty;

            try
            {

                if (crmEntity != null)
                {
                    csvLine = GetAttrAnsp(crmEntity);
                    
                }

            }
            finally
            {


            }

            return csvLine;
        }

        private String GetAttr(Entity crmEntity)
        {
            StringBuilder clipDataReturn = new StringBuilder();

            PropertyIterator<CrmName, DokumentInfo>.Iterate((attr, prop) =>
            {
                
                if (crmEntity.Attributes.Contains(attr.Name))
                {
                    clipDataReturn.Append(crmEntity.Attributes[attr.Name].ToString());
                }
                else
                {
                    clipDataReturn.Append(String.Empty);
                }

                WriteSeparatorOrHeader(clipDataReturn, attr.Name);
            });

            return clipDataReturn.ToString();

        }
        private String GetAttrAnsp(Entity crmEntity)
        {
            StringBuilder clipDataReturn = new StringBuilder();

            PropertyIterator<CrmNameAnspr, DokumentInfo>.Iterate((attr, prop) =>
            {
                
                if (crmEntity.Attributes.Contains(attr.Name))
                {
                    clipDataReturn.Append(crmEntity.Attributes[attr.Name].ToString());
                }
                else
                {
                    clipDataReturn.Append(String.Empty);
                }

                WriteSeparatorOrHeader(clipDataReturn, attr.Name);
            });

            return clipDataReturn.ToString();

        }

        private static void WriteSeparatorOrHeader(StringBuilder clipDataReturn, String headerName)
        {
            if (!String.IsNullOrEmpty(clipDataReturn.ToString()))
            {
                clipDataReturn.Append(";");
            }
        }

        private DokumentInfo ReadFromCSV(String stringLine)
        {
            DokumentInfo docInfo = new DokumentInfo();

            var csvData = stringLine.Split(';');
            int col = 0;

            PropertyIterator<CenteraName, DokumentInfo>.Iterate((attr, prop) =>
            {
                //Type type = prop.DeclaringType;
                //var value = Convert.ChangeType(csvData[col++], type);
                //prop.SetValue(docInfo, value);

                prop.SetValue(docInfo, csvData[col++]);
            });


            PropertyIterator<CrmName, DokumentInfo>.Iterate((attr, prop) =>
            {
                prop.SetValue(docInfo, csvData[col++]);
            });

            return docInfo;
        }

    }
}




