﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CenteraExporter.Classes
{
    [System.AttributeUsage(System.AttributeTargets.Property)]
    public class GeneralSharepointName : System.Attribute
    {
        public string Name;
        public string ListName;
        public string CrmPropName;

        public GeneralSharepointName()
        {
        }

        public GeneralSharepointName(string name, string listname, string crmpropname)
        {
            this.Name = name;
            this.ListName = listname;
            this.CrmPropName = crmpropname;
        }
    }

    public class EcmSharepointName : GeneralSharepointName
    {
        public EcmSharepointName() { }

        public EcmSharepointName(string name, string listname, string crmpropname)
            : base(name, listname, crmpropname)
        {
        }

    }

    public class SharepointName : GeneralSharepointName
    {
        public SharepointName() { }
        public SharepointName(string name, string listname, string crmpropname)
            : base(name, listname, crmpropname)
        {
        }
    }
}
