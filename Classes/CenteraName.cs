﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CenteraExporter.Classes
{
    [System.AttributeUsage(System.AttributeTargets.Property)]
    public class CenteraName : System.Attribute
    {
        public string Name;

        public CenteraName(string name)
        {
            this.Name = name;
        }
    }
}
