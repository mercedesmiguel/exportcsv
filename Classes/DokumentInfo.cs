﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CenteraExporter.Classes
{
    public class DokumentInfo
    {
        //Important to separate LEADS!!!
        [CrmName("companyname")]
        public string companyname { get; set; }
        //[CrmName("fullname")]
        //public string fullname { get; set; }
        [CrmName("subject")]
        public string subject { get; set; }

        [CrmNameAnspr("firstname")]
        public string firstname { get; set; }
        [CrmNameAnspr("lastname")]
        public string lastname { get; set; }
        

    }

}
