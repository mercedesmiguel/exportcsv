﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace CenteraExporter.Classes
{
    public class PropertyIterator<AttributeType, ClassWithProperties> where AttributeType : System.Attribute
        where ClassWithProperties: class
    {
        public delegate void CallBack(AttributeType attr, PropertyInfo prop);

        public static void Iterate(CallBack callBack)
        {
            foreach (var property in typeof(ClassWithProperties).GetProperties())
            {
                foreach (var attr in System.Attribute.GetCustomAttributes(property))
                {
                    AttributeType attrType = attr as AttributeType;
                    if (attrType != null)
                    {
                        callBack(attrType, property);
                    }
                }
            }
        }
    }
}
