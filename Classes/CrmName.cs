﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CenteraExporter.Classes
{
    [System.AttributeUsage(System.AttributeTargets.Property)]
    public class CrmName : System.Attribute
    {
        public string Name;

        public CrmName(string name)
        {
            this.Name = name;
        }
    }



    [System.AttributeUsage(System.AttributeTargets.Property)]
    public class CrmNameAnspr : System.Attribute
    {
        public string Name;

        public CrmNameAnspr(string name)
        {
            this.Name = name;
        }
    }
}
