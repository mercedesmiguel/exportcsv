﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CenteraExporter.Classes
{
    public static class ConfigFactory
    {
        public static ProcessConfiguration WrapProdConfiguration(string orgName)
        {
            return new ProcessConfiguration
            {
                CrmServerUrl = ConfigurationManager.AppSettings["CrmServerUrl"],
                OrganizationName = orgName,
                CrmServiceUser = ConfigurationManager.AppSettings["CrmServiceUser"],
                CrmServiceUserDomain = ConfigurationManager.AppSettings["CrmServiceUserDomain"],
                CrmServiceUserKey = ConfigurationManager.AppSettings["CrmServiceUserKey"],
                CenteraCluster = ConfigurationManager.ConnectionStrings["CenteraClusterProd"].ConnectionString,

                DownloadFoalder = ConfigurationManager.ConnectionStrings["DownloadFolder"].ConnectionString,
                DownloadLogg = ConfigurationManager.ConnectionStrings["DownloadLogg"].ConnectionString,

                CrmXrm = "CrmXrmProd",
                ProzessFolder = ConfigurationManager.ConnectionStrings["ProzessFolder"].ConnectionString,
                LoggPath = ConfigurationManager.ConnectionStrings["LoggPath"].ConnectionString,
                StoreExe = ConfigurationManager.ConnectionStrings["StoreExe"].ConnectionString,
                ReadExe = ConfigurationManager.ConnectionStrings["ReadExe"].ConnectionString,
                WcfServiceUrl = ConfigurationManager.ConnectionStrings["CenteraClusterTest"].ConnectionString
            };
        }

        public static ProcessConfiguration WrapTestConfiguration(string orgName)
        {
            return new ProcessConfiguration
            {
                CrmServerUrl = ConfigurationManager.AppSettings["CrmServerUrl"],
                OrganizationName = orgName,
                CrmServiceUser = ConfigurationManager.AppSettings["CrmServiceUser"],
                CrmServiceUserDomain = ConfigurationManager.AppSettings["CrmServiceUserDomain"],
                CrmServiceUserKey = ConfigurationManager.AppSettings["CrmServiceUserKey"],
                CenteraCluster = ConfigurationManager.ConnectionStrings["CenteraClusterTest"].ConnectionString,
                CrmXrm = "CrmXrm",
                DownloadFoalder = ConfigurationManager.ConnectionStrings["DownloadFolder"].ConnectionString,
                DownloadLogg = ConfigurationManager.ConnectionStrings["DownloadLogg"].ConnectionString,
                ProzessFolder = ConfigurationManager.ConnectionStrings["ProzessFolder"].ConnectionString,
                LoggPath = ConfigurationManager.ConnectionStrings["LoggPath"].ConnectionString,
                StoreExe = ConfigurationManager.ConnectionStrings["StoreExe"].ConnectionString,
                ReadExe = ConfigurationManager.ConnectionStrings["ReadExe"].ConnectionString,
                WcfServiceUrl = ConfigurationManager.ConnectionStrings["CenteraClusterTest"].ConnectionString
            };
        }
    }
}
