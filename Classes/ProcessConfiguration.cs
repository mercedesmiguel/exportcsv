﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CenteraExporter.Classes
{
    public class ProcessConfiguration
    {
        public string CrmServerUrl { get; set; }
        public string OrganizationName { get; set; }
        public string CrmServiceUser { get; set; }
        public string CrmServiceUserDomain { get; set; }
        public string CrmServiceUserKey { get; set; }

        public string WcfServiceUrl { get; set; }
        public string CrmXrm { get; set; }

        public string CenteraCluster { get; set; }

        public string StoreExe { get; set; }
        public string ReadExe { get; set; }
        public string LoggPath { get; set; }
        public string DownloadFoalder { get; set; }
        public string DownloadLogg { get; set; }
        public string ProzessFolder { get; set; }
    }
}
